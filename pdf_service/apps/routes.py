from fastapi import APIRouter

from apps.generate.views import v1 as v1_generate
from apps.users.views import v1 as v1_user

applications = APIRouter()

applications.include_router(v1_generate, prefix="/generate/v1", tags=["generate"])
applications.include_router(v1_user, prefix="/users/v1", tags=["users"])
