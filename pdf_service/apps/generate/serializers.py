from datetime import date, datetime, time

from pydantic import BaseModel, Field
from typing import Optional, List


class BaseModelDataPage(BaseModel):
    """Для всех страниц - название """

    name_page: Optional[str] = Field(..., title='Название страницы')


class ProfileData(BaseModel):
    """данные профиля"""

    firstname: Optional[str] = Field(..., title='Имя')  # profile.firstname
    lastname: Optional[str] = Field(..., title='Фамилия')  # profile.lastname
    middlename: Optional[str] = Field(..., title='Отчество')  # profile.lastname


class SuperVisorNotes(BaseModel):
    """ Заметки руководителя процедуры (Комментарии) """

    author: Optional[str] = ''  # comment.author
    note: Optional[str] = ''  # comment.note
    date: Optional[date] = None  # comment.date


class CompanyData(BaseModel):
    """Дополнительная инфо о компании """

    forma_text: Optional[str] = Field(None)  # procedure.company_profile.forma_text
    profile_name: Optional[str] = Field(None)  # procedure.company_profile.profile.name


class ProceduresLot(BaseModel):
    """ Лоты процедуры """

    name: Optional[str] = Field(..., title='Наименование')  # lot.name
    description: Optional[str] = Field(None, title='Описание')  # lot.specifications
    amount: Optional[int] = Field(..., title='Количество')  # lot.amount
    unit: Optional[str] = Field(..., title='Единица')  # lot.unit
    delivery_date: Optional[date] = Field(None, title='Дата поставки')  # lot.delivery_date
    specifications: Optional[str] = Field(None, title='Спецификация')  # lot.specifications



class ParticipantList(BaseModel):
    """Уведомления для участников"""

    contractor_name: Optional[str] = Field(..., title='Участник процедуры')
    sent_at: Optional[date] = Field(..., title='Отправлен запрос')
    received_at: Optional[date] = Field(..., title='Доставлен запрос')
    responded_at: Optional[date] = Field(None, title='Отправлен ответ')
    status: Optional[str] = Field(..., title='Результат')


class ParticipantLog(BaseModel):
    name: Optional[str] = Field(..., title='Участник процедуры')
    datetime: Optional[datetime] = Field(..., title='Дата и время')
    status: Optional[str] = Field(..., title='Результат')


class ResponseTable(BaseModel):
    """ Ответы участников """
    contractor_name: Optional[str] = Field(..., title='Участник процедуры')
    lot_request_name: Optional[str] = Field(..., title='Лот')
    lot_request_amount: Optional[int] = Field(..., title='Кол-во')
    lot_response_amount: Optional[float] = Field(None, title='Предложено')
    lot_unit: Optional[str] = Field(..., title='Ед. изм.')
    lot_response_price: Optional[float] = Field(..., title='Цена за ед.')
    lot_response_price_total: Optional[float] = Field(..., title='Всего')
    lot_response_delivery_date: Optional[date] = Field(..., title='Дата')
    delivery_address: Field(..., title='Место поставки')


class BaseModelProcedure(BaseModel):
    """ Базис процедуры """

    title: Optional[str] = Field(..., title='Название процедуры')  # procedure.data.title
    text: Optional[str] = Field(..., title='Описание процедуры')  # procedure.data.text
    completed_at: Optional[date] = Field(None, title='Дата окончания процедуры')  # procedure.completed_at
    created_at: Optional[date] = Field(None, title='Дата создания процедуры')  # procedure.created_at
    published_at: Optional[date] = Field(None, title='Дата публикации процедуры')  # procedure.published_at
    okpd: Optional[OKPD] = Field(None, title='Код ОКПД')  # procedure.data.okpd


class OKPD(BaseModel):
    code: str
    text: str


class AdvancedModelProcedure(BaseModelProcedure):
    """ Дополненные данные процедуры """

    okpd_text: Optional[str] = Field(None, title='Код ОКПД')  # procedure.data.okpd_text
    manufacturer: Optional[bool] = Field(None, title='Производитель')  # procedure.data.manufacturer
    representative: Optional[bool] = Field(None, title='Представитель')  # procedure.data.representative
    intermediary: Optional[bool] = Field(None, title='Посредник')  # procedure.data.intermediary
    company: Optional[int] = Field(None, title='Организации')  # procedure.data.company
    individual: Optional[bool] = Field(None, title='Частные лица')  # procedure.data.individual
    verified: Optional[bool] = Field(None, title='Авторизованные')  # procedure.data.verified
    accredited: Optional[bool] = Field(None, title='Аккредитованные')  # procedure.data.accredited
    mode: Optional[str] = Field(None, title='Статус (открытая, закрытая)')  # procedure.data.mode
    country_of_origin: Optional[str] = Field('',  title='Страна происхождения')  # procedure.data.country_of_origin
    condition_of_payment: Optional[str] = Field('', title='Условия оплаты')  # procedure.data.condition_of_payment
    incoterms: Optional[str] = Field('', title='Инкотермс')  # procedure.data.incoterms
    delivery_address: Optional[str] = Field('', title='Адрес доставки')  # procedure.data.delivery_address
    validity_period_date: Optional[date] = Field(None, title='Срок действия предложения (до даты)')  # procedure.data.validity_period_date
    validity_period_days: Optional[int] = Field(None, title='Срок действия предложения (в днях)')  # procedure.data.validity_period_days
    delivery_date: Optional[date] = Field(None, title='Сроки поставки')  # procedure.data.delivery_date
    full_set: Optional[bool] = Field(False, title='Только комплектная поставка')  # procedure.data.full_set
    analog: Optional[bool] = Field(False, title='Разрешить предлагать аналоги')  # procedure.data.analog
    changing_number: Optional[bool] = Field(False, title='Разрешить замену количества')  # procedure.data.changing_number
    supervisor_notes: Optional[List[SuperVisorNotes]] = Field([], title='Комментарии')  # procedure.supervisor_notes
    lots: Optional[List[ProceduresLot]] = Field([], title='Лоты')  # procedure.data.lots
    participants_list: Optional[List[ParticipantList]] = Field([], 'Уведомления')  # report.participant_list


class FullModelProcedure(AdvancedModelProcedure):
    """ Расширенные данные процедуры """

    participants_log: Optional[List[ParticipantLog]] = Field([], 'Ответы участников')  # report.participant_log


class ParticipantResponse(BaseModel):
    """Ответы участников"""

    contractor_name: Optional[str] = Field(..., title='Участник процедуры')
    bet_at: Optional[date] = Field(..., title='Дата и время')
    amount: Optional[float] = Field(..., title='Цена')


class MessagePage(BaseModelDataPage, ProfileData):
    """ procedures/message.html """

    name_page: str = 'МОНИТОРГ: Сообщение'
    profile: Optional[OwnerData] = Field(..., title='Информация о владельце')  # profile


class ReductionReport(BaseModelDataPage, FullModelProcedure):
    """ procedures/reduction_report.html """

    name_page: str = 'МОНИТОРГ: Завершенный редукцион'


class ReductionReportParticipant(BaseModelDataPage, FullModelProcedure):
    """ procedures/reduction_report_participant.html """

    name_page: str = 'МОНИТОРГ: Завершенный редукцион'


class TradeProcedureReport(BaseModelDataPage, AdvancedModelProcedure):
    """procedures/trade_procedure_report.html"""

    name_page: str = 'МОНИТОРГ: Завершенная торговая процедура'
    company_data: Optional[CompanyData] = Field(None, title='Информация о компании')  # procedure.data.company
    owner_data: Optional[ProfileData] = Field(..., title='Информация о создателе')  # procedure.owner_profile
    vat: Optional[str] = Field(None, title="Налоги")  # procedure.data.vat
    no_vat: Optional[bool] = Field(None, title="Облагается налогом")  # procedure.data.no_vat
    currency: Optional[str] = Field('', title="Валюта")  # procedure.data.currency
    participant_log: Optional[List[ParticipantLog]] = Field([], title="Уведомления")  # report.participant_log
    response_table: Optional[List[ResponseTable]] = Field([], title="Ответы участников")  # report.response_table


class TradeProcedureReportParticipant(BaseModelDataPage, FullModelProcedure):
    """procedures/trade_procedure_report_participant.html"""

    name_page: str = 'МОНИТОРГ: Завершенная торговая процедура'
    vat: Optional[str] = Field(None, title="Налоги")  # procedure.data.vat
    no_vat: Optional[bool] = Field(None, title="Облагается налогом")  # procedure.data.no_vat
    currency: Optional[str] = Field('', title="Валюта")  # procedure.data.currency
    response_table: Optional[List[ResponseTable]] = Field([], title="Ответы участников")  # report.response_table


class TradeProcedureResponse(BaseModelDataPage):
    """procedures/trade_procedure_response.html"""

    name_page: str = 'МОНИТОРГ: Ответ на процедуру'


class OrgStats(BaseModel):
    published_procedures_count: Optional[int] = Field(None, title='Открыто процедур')  # stats.published_procedures_count
    completed_procedures_count: Optional[int] = Field(None, title='Проведено процедур')  # stats.completed_procedures_count


class Schedule(BaseModel):
    day: Optional[str] = Field('', title='День')
    _from: Optional[time] = Field('', title='Время с')
    to: Optional[time] = Field('', title='Время по')


class Address(BaseModel):
    country: Optional[str] = Field('', title='Страна')  # address.country
    region: Optional[str] = Field('', title='Регион')  # address.region
    settlement: Optional[str] = Field('', title='Поселение')  # address.settlement
    postcode: Optional[int] = Field('', title='Индекс')  # address.postcode
    schedule_ref: Optional[Schedule] = Field(None, title='График работы')  # address.schedule_ref


class OrgHeadOfficeData(BaseModel):
    """Главный офис"""
    legalAddress: Optional[Address] = Field(..., title='Юридический адрес')  # org.profile.head_office.legalAddress
    actualAddress: Optional[Address] = Field(..., title='Фактический адрес')  # org.profile.head_office.actualAddress
    mailingAddress: Optional[Address] = Field(..., title='Почтовый адрес')  # org.profile.head_office.mailingAddress


class ProfileOrgData(BaseModel):
    name: Optional[str] = Field(..., title='Наименование организации')  # profile.name
    site: Optional[str] = Field(None, title='Сайт')  # profile.url
    email: Optional[str] = Field(None, title='Почта')  # profile.email
    phone: Optional[str] = Field(None, title='Телефон')  # profile.phone
    kpp: Optional[str] = Field(None, title='КПП')  # profile.kpp
    ogrn: Optional[str] = Field(None, title='ОГРН')  # profile.ogrn
    info: Optional[str] = Field(None, title='Информация')  # profile.info
    head_office: Optional[OrgHeadOfficeData] = Field(None, title='Главный офис')  # profile.head_office


class Branch(BaseModel):
    """Филиалы"""
    profile: Optional[ProfileOrgData] = Field(..., title='Профиль')  # org.profile


class Code(BaseModel):
    """Продукты и услуги"""
    _id: int
    text: str


class ContactList(ProfileData):  # <- Profile data is contact.user.profile
    department: str = ''  # contact.department
    appointment: str = '' # contact.appointment
    email: str = ''  # contact.email
    phone: str = ''  # contact.phone
    additionalPhone: str = ''  # contact.additionalPhone


class AccreditedList(BaseModel):
    company_name: str = ''   # accredited.company.profile.name
    profile: Optional[ProfileData] = None  # accredited.user.profile.lastname accredited.user.profile.firstname  accredited.user.profile.middlename
    accred_at: Optional[date] = None


class ProfileOrg(BaseModelDataPage, OrgStats):
    """profile/org.html"""

    name_page: str = 'МОНИТОРГ: Профиль организации'
    logo_url: Optional[str] = Field(..., title='Лого организации')  # org.logo_url
    profile: Optional[ProfileOrgData] = Field(..., title='Профиль')  # org.profile
    authorized: Optional[bool] = Field(False, title='Авторизован')  # org.authorized
    inn: Optional[int] = Field(..., title='ИНН')  # org.inn
    branches: Optional[List[Branch]] = Field([], title='Филиалы')  # org.profile.branches
    okdp: Optional[Code] = Field([])  # okdp
    okpd: Optional[Code] = Field([])  # okpd
    counter: Optional[int] = Field(None, title='Всего')  # counter
    procedure_list: Optional[List[BaseModelProcedure]] = Field([], title='Процедуры')  # procedure_list
    contact_list: Optional[List[ContactList]] = Field([], title='Контакты')  # contact_list (contact.user.profile)
    accredited_list: Optional[List[AccreditedList]] = Field([], title='Аккредитация')  # accredited_list


class ProfileUser(BaseModelDataPage, ProfileData, OrgStats, OrgHeadOfficeData):
    """profile/user.html"""

    name_page: str = 'МОНИТОРГ: Профиль пользователя'
    url: Optional[str] = Field(None, title='Сайт')  # user.profile.url
    email: Optional[str] = Field(None, title='Почта')  # user.profile.email
    phone: Optional[str] = Field(None, title='Телефон')  # user.profile.phone
    info: Optional[str] = Field(None, title='Информация')  # user.profile.info
    procedure_list: Optional[List[BaseModelProcedure]] = Field([], title='Процедуры')  # procedure_list
    accredited_list: Optional[List[AccreditedList]] = Field([], title='Аккредитация')  # accredited_list


class ProfileVerification(BaseModel):
    """profile/verification.html"""

    name_page: str = 'Платёжные данные для верификации'