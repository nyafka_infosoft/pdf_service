from fastapi import APIRouter, Depends, HTTPException, Request
from fastapi.responses import HTMLResponse
from sqlalchemy.orm import Session

# from core.application import CustomApp
from core.integration import Integration
from core.server.db.base import db

v1 = APIRouter()


@v1.get("/generate/{template_name}", response_class=HTMLResponse)
async def get_data(*, db: Session = Depends(db), request: Request,  session_key: str, template_name: str):

    # проверяем есть ли такой user
    user_id = Integration().get_user_id(session_key)
    if not user_id:
        raise HTTPException(status_code=400, detail="Inactive user")

    # получаем данные по пользователю
    user = Integration().get_user(user_id)

    return _app.templates.TemplateResponse(f'{template_name}', {"request": request, "response": response})
