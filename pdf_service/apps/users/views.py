from fastapi import APIRouter
from pydantic import Field

from core.server.db.base import db
from core.var_type import OID

from .serializers import UserInfo


v1 = APIRouter()


@v1.get("/{user_id}", response_model=UserInfo)
async def get_user(user_id: OID = Field(alias="_id")):
    user = await db.users.find_one({'_id': user_id})
    return user.items()