from pydantic import BaseModel, EmailStr, Field

from core.var_type import OID


class UserInfo(BaseModel):
    _id: OID = Field(alias="_id")
    profile: dict
    settings: dict
    del_status: str
    tutorial_passed: bool
    email: EmailStr
    code: str
    infixes: str = ''
    prefixes: str = ''
    rel: dict
