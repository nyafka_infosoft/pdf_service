import sentry_sdk
from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse
from fastapi.templating import Jinja2Templates
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from starlette.exceptions import HTTPException as StarletteHTTPException

from core.server import MIDDLEWARES, MOUNTS
from core.routes import apps
from core.settings import settings, AppSettings
from core.service import service_route

_app = None

async def http_exception_handler(request, exc):
    return PlainTextResponse(str(exc.detail), status_code=exc.status_code)

async def validation_exception_handler(request, exc):
    return PlainTextResponse(str(exc), status_code=400)


class CustomApp(FastAPI):
    templates = Jinja2Templates(directory=settings.TEMPLATES_DIR)


def get_app():
    global _app
    if not _app:
        _app = CustomApp(**AppSettings().dict(), middleware=MIDDLEWARES)

        _app.exception_handlers[StarletteHTTPException] = http_exception_handler
        _app.exception_handlers[RequestValidationError] = validation_exception_handler

        _app.include_router(service_route)
        _app.include_router(apps)

        if settings.SENTRY_DSN:
            sentry_sdk.init(dsn=settings.SENTRY_DSN)
            _app.add_middleware(SentryAsgiMiddleware)
        for _mount in MOUNTS:
            _app.mount(*_mount)
        # print(_app)
        # db.init_app(_app)
    return _app