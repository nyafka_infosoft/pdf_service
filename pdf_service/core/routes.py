from fastapi import APIRouter
from apps.routes import applications

apps = APIRouter()

apps.include_router(applications, prefix="/api")
