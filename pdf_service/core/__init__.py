from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette_prometheus import PrometheusMiddleware


ORIGINS = [
    "http://localhost",
    "http://localhost:8000",
]

MIDDLEWARES = (
    Middleware(PrometheusMiddleware),
    Middleware(
        CORSMiddleware,
        allow_origins=ORIGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    ),
)