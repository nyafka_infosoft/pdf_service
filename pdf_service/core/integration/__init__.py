import os

import redis
from bson.objectid import ObjectId
from pymongo import MongoClient

from core.settings import settings

e = os.environ.get


class Integration:
    CLIENT = MongoClient(e('USERS_DB')).crm_trade
    REDIS_CLIENT = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

    def __init__(self):
        pass

    def check_user(self, email):
        return bool(self.CLIENT.users.find_one({'email': email}))

    def get_user_id(self, session_key):
        result = self.REDIS_CLIENT.get(f'crmt:session:{session_key}')
        if result:
            return result.decode("utf-8")

    def get_user(self, user_id):
        return self.CLIENT.users.find_one({'_id': ObjectId(user_id)})
