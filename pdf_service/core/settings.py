import os
from pathlib import Path

from pydantic import BaseSettings, SecretStr, DirectoryPath

BASE_DIR = Path(__file__).parent.parent
UP_DIR = BASE_DIR.parent.parent

e = os.environ.get


def get_env_path():
    _path = (
        (BASE_DIR / ".env.local"),
        (BASE_DIR / ".env"),
        (BASE_DIR.parent / ".env.local"),
        (BASE_DIR.parent / ".env"),
    )
    for _p in _path:
        if _p.exists():
            return _p.resolve()


class AppSettings(BaseSettings):
    title: str = "PDF service"
    version: str = "0.0.1"
    description: str = ""
    docs_url: str = "/swagger"


class RunSettings(BaseSettings):
    reload: bool = bool(e("DEBUG"))
    debug: bool = bool(e("DEBUG"))
    host: str = e("HOST", "0.0.0.0")
    port: int = int(e("PORT", 8000))
    log_level: str = "debug"

    class Config:
        env_file = get_env_path()


class Settings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 8000
    LOG_LEVEL: str = "debug"
    RELOAD: bool = True
    DEBUG: bool = True
    SENTRY_DSN: str = None
    DB_PASSWORD: SecretStr = "crm_trade"
    DB_NAME: str = "crm_trade"
    DATABASE_URL: str = "mongodb://localhost:27017"
    DB_DSN: str = f"mongodb://{DB_NAME}:{DB_PASSWORD}@localhost:27017/{DB_NAME}"
    DB_POOL_MIN_SIZE: int = 1
    DB_POOL_MAX_SIZE: int = 16
    DB_ECHO: bool = True
    DB_SSL: str = None
    DB_USE_CONNECTION_FOR_REQUEST: bool = True
    DB_RETRY_LIMIT: int = 1
    DB_RETRY_INTERVAL: int = 1
    TEMPLATES_DIR: DirectoryPath = UP_DIR / "templates"
    MEDIA_PATH: DirectoryPath = BASE_DIR / "media"
    STATIC_PATH: DirectoryPath = BASE_DIR / "static"
    SECRET_KEY: str = "919dff2ce5d30bbf5690e3c0b2a578be3a4cd8361e6866ca2624ced99bbfc1bd"
    AUTH_ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30

    REDIS_HOST: str = '0.0.0.0'
    REDIS_PORT: int = 6379
    REDIS_DB: str = ''

    class Config:
        env_file = get_env_path()


settings = Settings()
