import motor.motor_asyncio

from core.settings import settings



client = motor.motor_asyncio.AsyncIOMotorClient(
    settings.DB_DSN, uuidRepresentation="standard"
)
db = client[settings.DB_NAME]